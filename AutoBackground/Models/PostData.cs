﻿namespace AutoBackground.Models
{
    public class PostData
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public string Url { get; set; }
        public bool Over_18 { get; set; }
    }
}
