﻿namespace AutoBackground.Models
{
    public class Listing
    {
        public string Kind { get; set; }
        public ListingData Data { get; set; }
    }
}
