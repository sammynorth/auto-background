﻿namespace AutoBackground.Models
{
    public class Post
    {
        public string Kind { get; set; }
        
        public PostData Data { get; set; }
    }
}
