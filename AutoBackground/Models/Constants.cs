﻿namespace AutoBackground.Models
{
    public static class Constants
    {
        public struct Settings
        {
            public const string AppSettingSectionName = "appSettings";
            public const char AppSettingMultiValueDelimiter = ',';

            public const string SubredditUrls = "SubredditUrls";
            public const string ImageNameHistory = "ImageNameHistory";
            public const string AllowNSFW = "AllowNSFW";
            public const string ImageDownloadDirectory = "ImageDownloadDirectory";
            public const string ImageFavoritesDirectory = "ImageFavoritesDirectory";
            public const string ImageHistoryCountLimit = "ImageHistoryCountLimit";
            public const string FavoritesFrequency = "FavoritesFrequency";
            public const string TimeIntervalMinutes = "TimeIntervalMinutes";

            public const string IconFileName = "starry-night.ico";
            public const string IconText = "Auto Background";
        }
    }
}