﻿namespace AutoBackground.Models
{
    public class UserSettings
    {
        public string SubredditUrls { get; set; }
        public string ImageNameHistory { get; set; }
        public bool AllowNSFW { get; set; }
        public int ImageHistoryCountLimit { get; set; }
        public int TimeIntervalMinutes { get; set; }
        public int FavoritesFrequency { get; set; }
    }
}
