﻿using System.Collections.Generic;

namespace AutoBackground.Models
{
    public class ListingData
    {
        public ICollection<Post> Children { get; set; }
    }
}
