﻿using AutoBackground.Logic.Concrete;
using AutoBackground.Logic.Interface;

namespace AutoBackground.Factory
{
    public static class SettingFactory
    {
        public static ISettingsIO GetAppSettingsIO()
        {
            return new AppSettingsIO();
        }
        public static ISettingsIO GetUserSettingsIO()
        {
            return new UserSettingsIO();
        }
    }
}