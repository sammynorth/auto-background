﻿using AutoBackground.Factory;
using AutoBackground.Logic.Interface;
using AutoBackground.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace AutoBackground.Logic
{
    public class ImageDownloader
    {
        ISettingsIO UserSettingsIO { get; }
        ISettingsIO AppSettingsIO { get; }
        private bool _allowNsfw;
        private bool _successfulImageSet = false;
        public ImageDownloader()
        {
            UserSettingsIO = SettingFactory.GetUserSettingsIO();
            AppSettingsIO = SettingFactory.GetAppSettingsIO();
        }
        public void Execute()
        {
            _successfulImageSet = false;
            _allowNsfw = UserSettingsIO.GetAppSettingAsBool(Constants.Settings.AllowNSFW);

            while (!_successfulImageSet)
                DecideOnImageAndSetBackground();
        }

        private void DecideOnImageAndSetBackground()
        {
            var favoritesDirectoryInfo = Directory.CreateDirectory(AppSettingsIO.GetPathAppSettingWithDocumentsDirectory(Constants.Settings.ImageFavoritesDirectory));
            var filesInFavoritesDirectory = Directory.GetFiles(AppSettingsIO.GetPathAppSettingWithDocumentsDirectory(Constants.Settings.ImageFavoritesDirectory));
            var random = new Random();
            var newOrFavs = random.Next(0, 100);
            var favoritesFrequency = UserSettingsIO.GetAppSettingAsInt(Constants.Settings.FavoritesFrequency);
            var downloadFrequency = 100 - favoritesFrequency;
            if (newOrFavs < downloadFrequency || filesInFavoritesDirectory == null || filesInFavoritesDirectory.Length == 0)
                DownloadAndSet();
            else
                SetFromFavorites(favoritesDirectoryInfo, filesInFavoritesDirectory);
        }

        private void SetFromFavorites(DirectoryInfo favoritesDirectoryInfo, string[] filesInDirectory)
        {
            try
            {
                var favoritesDirectoryString = AppSettingsIO.GetPathAppSettingWithDocumentsDirectory(Constants.Settings.ImageFavoritesDirectory);
                var lastImageNames = UserSettingsIO.GetAppSetting(Constants.Settings.ImageNameHistory);
                var favoritesFrequency = UserSettingsIO.GetAppSettingAsInt(Constants.Settings.FavoritesFrequency);
                if (filesInDirectory.Length > 1)
                {
                    var imageHistoryNamesSplit = lastImageNames.Split(Constants.Settings.AppSettingMultiValueDelimiter);
                    var currentImageName = imageHistoryNamesSplit[imageHistoryNamesSplit.Length - 1];
                    filesInDirectory = filesInDirectory.Where(x => x.Replace(favoritesDirectoryString, "") != currentImageName).ToArray();
                }
                if (favoritesFrequency != 100)
                    filesInDirectory = filesInDirectory.Where(x => !lastImageNames.Contains(x.Replace(favoritesDirectoryString, ""))).ToArray();
                if (filesInDirectory.Any())
                {
                    var imageRandom = new Random();
                    var whichImageRandom = imageRandom.Next(0, filesInDirectory.Length);
                    var randomFavoriteImagePath = filesInDirectory.ElementAt(whichImageRandom);

                    var index = randomFavoriteImagePath.IndexOf(favoritesDirectoryString);
                    var fileNameOnly = randomFavoriteImagePath.Substring(index + favoritesDirectoryString.Length);
                    var fullPath = Path.Combine(favoritesDirectoryString, fileNameOnly);
                    UserSettingHelper.RemoveDuplicatesFromAppSetting(Constants.Settings.ImageNameHistory, fileNameOnly);
                    UserSettingHelper.AppendAppSetting(Constants.Settings.ImageNameHistory, fileNameOnly);
                    UserSettingHelper.TrimAppSettingToOnlyNValues(Constants.Settings.ImageNameHistory, UserSettingsIO.GetAppSettingAsInt(Constants.Settings.ImageHistoryCountLimit));
                    Wallpaper.Set(fullPath, Wallpaper.Style.Fill);
                    _successfulImageSet = true;
                }
                else
                    DecideOnImageAndSetBackground();
            }
            catch (Exception e)
            {
                new Logger().LogException(e);
            }
        }

        private void DownloadAndSet(string afterId = null)
        {
            var splitSubredditUrls = UserSettingsIO.GetAppSetting(Constants.Settings.SubredditUrls).Split(Constants.Settings.AppSettingMultiValueDelimiter);
            Random ran = new Random();
            var randomSubreddit = splitSubredditUrls[ran.Next(0, splitSubredditUrls.Length)];
            var url = randomSubreddit + ".json";
            if (!string.IsNullOrWhiteSpace(afterId))
            {
                url += "?count=25&after=" + afterId;
            }

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var client = new RestClient(url);
            RestRequest request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            IRestResponse response = client.Execute(request);
            Listing json = JsonConvert.DeserializeObject<Listing>(response.Content);


            CreateDirectoryAndDownloadFile(json);
            new CleanupDownloads().Execute();
        }

        private void CreateDirectoryAndDownloadFile(Listing json)
        {
            string previousId = "";
            try
            {
                var imageDirectoryPath = AppSettingsIO.GetPathAppSettingWithDocumentsDirectory(Constants.Settings.ImageDownloadDirectory);
                var imageDirectoryInfo = Directory.CreateDirectory(imageDirectoryPath);
                var lastImageNames = UserSettingsIO.GetAppSetting(Constants.Settings.ImageNameHistory);
                var viablePosts = json.Data.Children.Where(x => x.Data.Name.StartsWith("t3_") && !lastImageNames.Contains(x.Data.Name) && !string.IsNullOrWhiteSpace(x.Data.Url) && (!x.Data.Over_18 || _allowNsfw)).ToList();

                foreach (var post in viablePosts)
                {
                    previousId = post.Data.Name;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    var req = (HttpWebRequest)WebRequest.Create(post.Data.Url);
                    req.Timeout = 2000;
                    var resp = (HttpWebResponse)req.GetResponse();

                    using (var stream = resp.GetResponseStream())
                    {
                        var imageFormat = ImageHelper.GetImageFormat(stream);
                        if (imageFormat != ImageFormat.unknown)
                        {
                            var imageName = post.Data.Name + "." + imageFormat.ToString();
                            if (!lastImageNames.Contains(imageName))
                            {
                                var newFileAndPath = Path.Combine(imageDirectoryPath, imageName);
                                using (var webClient = new WebClient())
                                {
                                    webClient.DownloadFile(post.Data.Url, newFileAndPath);
                                }
                                _successfulImageSet = File.Exists(newFileAndPath);
                                if (_successfulImageSet)
                                {
                                    UserSettingHelper.AppendAppSetting(Constants.Settings.ImageNameHistory, imageName);
                                    UserSettingHelper.TrimAppSettingToOnlyNValues(Constants.Settings.ImageNameHistory, UserSettingsIO.GetAppSettingAsInt(Constants.Settings.ImageHistoryCountLimit));
                                    Wallpaper.Set(newFileAndPath, Wallpaper.Style.Fill);
                                }
                            }
                            break;
                        }
                    }
                }
                if (!_successfulImageSet)
                {
                    var lastChild = json.Data.Children.LastOrDefault();
                    if (lastChild != null)
                    {
                        DownloadAndSet(lastChild.Data.Name);
                    }
                }
            }
            catch (Exception e)
            {
                new Logger().LogException(e);
                DownloadAndSet(previousId);
            }
        }


    }
}
