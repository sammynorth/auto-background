﻿using AutoBackground.Models;
using System;
using System.Configuration;

namespace AutoBackground.Logic.Concrete
{
    public class AppSettingsIO : BaseSettingsIO
    {
        public override string GetAppSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public override void SaveAppSetting(string key, string value)
        {
            try
            {
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings[key].Value = value ?? string.Empty;
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(Constants.Settings.AppSettingSectionName);
            }
            catch (Exception e)
            {
                new Logger().LogException(e);
            }
        }
    }
}