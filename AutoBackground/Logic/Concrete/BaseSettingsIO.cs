﻿using AutoBackground.Logic.Interface;
using System;
using System.IO;
using System.Reflection;

namespace AutoBackground.Logic.Concrete
{
    public abstract class BaseSettingsIO : ISettingsIO
    {
        public abstract string GetAppSetting(string key);
        public abstract void SaveAppSetting(string key, string value);

        public bool GetAppSettingAsBool(string key)
        {
            var appSettingValue = GetAppSetting(key);
            if (bool.TryParse(appSettingValue, out var returnValue))
                return returnValue;
            throw new Exception("The config setting for '" + key + "' was not a boolean value");
        }

        public int GetAppSettingAsInt(string key)
        {
            var appSettingAsString = GetAppSetting(key);
            if (int.TryParse(appSettingAsString, out var parsedInt))
                return parsedInt;
            throw new Exception("The config setting for '" + key + "' was not a valid integer");
        }

        public string GetPathAppSettingWithDocumentsDirectory(string key)
        {
            var appSettingAsString = GetAppSetting(key);
            var appDocumentsDirectory = Path.Combine(GetUserSettingsDirectory(), appSettingAsString);
            return appDocumentsDirectory + "\\";
        }

        protected string GetUserSettingsDirectory()
        {
            var myDocumentsEnvironmentPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var appName = Assembly.GetExecutingAssembly().GetName().Name;
            return Path.Combine(myDocumentsEnvironmentPath, appName);
        }
    }
}
