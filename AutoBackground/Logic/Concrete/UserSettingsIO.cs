﻿using AutoBackground.Factory;
using AutoBackground.Models;
using Newtonsoft.Json;
using System;
using System.IO;

namespace AutoBackground.Logic.Concrete
{
    public class UserSettingsIO : BaseSettingsIO
    {
        public override string GetAppSetting(string key)
        {
            var userSettings = GetUserSettings();
            return userSettings.GetType().GetProperty(key).GetValue(userSettings, null).ToString();
        }

        private UserSettings GetUserSettings()
        {
            var filePath = GetUserSettingFileFullPath();
            return !File.Exists(filePath) ? CreateDefaultUserSettingsFile(filePath) : LoadAndMapUserSettings(filePath);
        }

        private string GetUserSettingFileFullPath()
        {
            var userDirectoryPath = GetUserSettingsDirectory();
            var filePath = Path.Combine(userDirectoryPath, "settings.json");
            return filePath;
        }

        private UserSettings LoadAndMapUserSettings(string filePath)
        {
            var userSettings = new UserSettings();
            try
            {
                userSettings = JsonConvert.DeserializeObject<UserSettings>(File.ReadAllText(filePath));
            }
            catch (Exception e)
            {
                new Logger().LogException(e);
            }
            return userSettings;
        }

        private UserSettings CreateDefaultUserSettingsFile(string filePath)
        {
            var userSettings = new UserSettings();
            try
            {
                var appSettingsIO = SettingFactory.GetAppSettingsIO();

                userSettings.AllowNSFW = appSettingsIO.GetAppSettingAsBool(Constants.Settings.AllowNSFW);
                userSettings.ImageHistoryCountLimit = appSettingsIO.GetAppSettingAsInt(Constants.Settings.ImageHistoryCountLimit);
                userSettings.ImageNameHistory = appSettingsIO.GetAppSetting(Constants.Settings.ImageNameHistory);
                userSettings.SubredditUrls = appSettingsIO.GetAppSetting(Constants.Settings.SubredditUrls);
                userSettings.TimeIntervalMinutes = appSettingsIO.GetAppSettingAsInt(Constants.Settings.TimeIntervalMinutes);
                userSettings.FavoritesFrequency = appSettingsIO.GetAppSettingAsInt(Constants.Settings.FavoritesFrequency);

                Directory.CreateDirectory(GetUserSettingsDirectory());

                SaveSettings(userSettings);
            }
            catch (Exception e)
            {
                new Logger().LogException(e);
            }
            return userSettings;
        }

        private void SaveSettings(UserSettings userSettings)
        {
            try
            {
                var filePath = GetUserSettingFileFullPath();
                var jsonString = JsonConvert.SerializeObject(userSettings, Formatting.Indented);
                using (var f = (TextWriter)File.CreateText(filePath))
                {
                    f.WriteLine(jsonString);
                    f.Close();
                }
            }
            catch (Exception e)
            {
                new Logger().LogException(e);
            }
        }

        public override void SaveAppSetting(string key, string value)
        {
            try
            {
                var userSettings = GetUserSettings();
                var modelType = userSettings.GetType();
                var property = modelType.GetProperty(key);
                var typeOfProperty = property.PropertyType;
                userSettings.GetType().GetProperty(key).SetValue(userSettings, Convert.ChangeType(value, typeOfProperty));
                SaveSettings(userSettings);
            }
            catch (Exception e)
            {
                new Logger().LogException(e);
            }
        }
    }
}
