﻿namespace AutoBackground.Logic.Interface
{
    public interface ISettingsIO
    {
        void SaveAppSetting(string key, string value);
        string GetAppSetting(string key);
        bool GetAppSettingAsBool(string key);
        int GetAppSettingAsInt(string key);
        string GetPathAppSettingWithDocumentsDirectory(string key);
    }
}
