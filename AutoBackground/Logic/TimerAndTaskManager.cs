﻿using AutoBackground.Factory;
using AutoBackground.Logic.Interface;
using AutoBackground.Models;
using System;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace AutoBackground.Logic
{
    public static class TimerAndTaskManager
    {
        static ISettingsIO UserSettingsIO { get; }
        static TimerAndTaskManager()
        {
            UserSettingsIO = SettingFactory.GetUserSettingsIO();
        }
        private static DispatcherTimer dispatcherTimer;

        private static void StartTimer()
        {
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, UserSettingsIO.GetAppSettingAsInt(Constants.Settings.TimeIntervalMinutes), 0);
            dispatcherTimer.Start();
        }

        public static void StartOrRestartDownloadLogicOnTimer()
        {
            if (dispatcherTimer != null)
                dispatcherTimer.Stop();
            StartTimer();
            RunImageDownloadingLogic();
        }

        private static async void RunImageDownloadingLogic()
        {
            await Task.Run(() =>
            {
                var _imageDownloader = new ImageDownloader();
                _imageDownloader.Execute();
            });
        }

        private static void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            RunImageDownloadingLogic();
        }
    }
}
