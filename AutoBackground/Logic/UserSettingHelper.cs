﻿using AutoBackground.Factory;
using AutoBackground.Logic.Interface;
using AutoBackground.Models;
using System.Linq;

namespace AutoBackground.Logic
{
    public static class UserSettingHelper
    {
        static ISettingsIO UserSettingsIO { get; }

        static UserSettingHelper()
        {
            UserSettingsIO = SettingFactory.GetUserSettingsIO();
        }

        internal static void AppendAppSetting(string key, string value)
        {
            var appSetting = UserSettingsIO.GetAppSetting(key);
            if (!string.IsNullOrWhiteSpace(appSetting))
                appSetting += Constants.Settings.AppSettingMultiValueDelimiter;
            appSetting += value ?? string.Empty;
            UserSettingsIO.SaveAppSetting(key, appSetting);
        }

        internal static void TrimAppSettingToOnlyNValues(string key, int n)
        {
            var appSettingValue = UserSettingsIO.GetAppSetting(key);
            var splitSetting = appSettingValue.Split(Constants.Settings.AppSettingMultiValueDelimiter).Where(x => !string.IsNullOrEmpty(x)).ToArray();
            if (splitSetting.Length > n)
            {
                var amountGreaterThanN = splitSetting.Length - n;
                var newAppSettingHistoryValue = string.Empty;
                var maxLoopCount = splitSetting.Length - 1;
                for (int i = amountGreaterThanN; i <= maxLoopCount; i++)
                {
                    newAppSettingHistoryValue += splitSetting[i];
                    if (i != maxLoopCount)
                        newAppSettingHistoryValue += Constants.Settings.AppSettingMultiValueDelimiter;
                }
                UserSettingsIO.SaveAppSetting(key, newAppSettingHistoryValue);
            }
        }

        public static void RemoveDuplicatesFromAppSetting(string key, string duplicateValueToRemoveFromDelimitedValues)
        {
            var appSettingValue = UserSettingsIO.GetAppSetting(key);
            var newAppSettingValue = appSettingValue.Replace(Constants.Settings.AppSettingMultiValueDelimiter + duplicateValueToRemoveFromDelimitedValues, string.Empty);
            newAppSettingValue = newAppSettingValue.Replace(duplicateValueToRemoveFromDelimitedValues, string.Empty);
            UserSettingsIO.SaveAppSetting(key, newAppSettingValue);
        }
    }
}
