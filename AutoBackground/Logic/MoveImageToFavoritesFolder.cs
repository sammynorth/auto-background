﻿using AutoBackground.Factory;
using AutoBackground.Logic.Interface;
using AutoBackground.Models;
using System;
using System.IO;

namespace AutoBackground.Logic
{
    public class MoveImageToFavoritesFolder
    {
        ISettingsIO UserSettingsIO { get; }
        ISettingsIO AppSettingsIO { get; }
        public MoveImageToFavoritesFolder()
        {
            UserSettingsIO = SettingFactory.GetUserSettingsIO();
            AppSettingsIO = SettingFactory.GetAppSettingsIO();
        }

        public void Execute()
        {
            try
            {
                var imageDirectory = AppSettingsIO.GetPathAppSettingWithDocumentsDirectory(Constants.Settings.ImageDownloadDirectory);
                var imageHistoryNames = UserSettingsIO.GetAppSetting(Constants.Settings.ImageNameHistory);
                var imageFavoritesDirectory = AppSettingsIO.GetPathAppSettingWithDocumentsDirectory(Constants.Settings.ImageFavoritesDirectory);

                var imageHistoryNamesSplit = imageHistoryNames.Split(Constants.Settings.AppSettingMultiValueDelimiter);
                var currentImageName = imageHistoryNamesSplit[imageHistoryNamesSplit.Length - 1];

                var filesInImageDirectory = Directory.GetFiles(imageDirectory);
                foreach (var filePathAndName in filesInImageDirectory)
                {
                    var index = filePathAndName.IndexOf(imageDirectory);
                    var fileNameOnly = filePathAndName.Substring(index + imageDirectory.Length);
                    var newName = filePathAndName.Replace(imageDirectory, imageFavoritesDirectory);
                    if (fileNameOnly == currentImageName && File.Exists(@filePathAndName) && !File.Exists(newName))
                    {
                        File.Move(@filePathAndName, @newName);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                new Logger().LogException(e);
            }
        }
    }
}