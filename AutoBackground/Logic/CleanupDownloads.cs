﻿using AutoBackground.Factory;
using AutoBackground.Logic.Interface;
using AutoBackground.Models;
using System;
using System.IO;

namespace AutoBackground.Logic
{
    public class CleanupDownloads
    {
        ISettingsIO UserSettingsIO { get; }
        ISettingsIO AppSettingsIO { get; }
        public CleanupDownloads()
        {
            UserSettingsIO = SettingFactory.GetUserSettingsIO();
            AppSettingsIO = SettingFactory.GetAppSettingsIO();
        }

        public void Execute()
        {
            try
            {
                var imageDirectory = AppSettingsIO.GetPathAppSettingWithDocumentsDirectory(Constants.Settings.ImageDownloadDirectory);
                var imageHistoryNames = UserSettingsIO.GetAppSetting(Constants.Settings.ImageNameHistory);

                var filesInDirectory = Directory.GetFiles(imageDirectory);
                foreach (var filePathAndName in filesInDirectory)
                {
                    var index = filePathAndName.IndexOf(imageDirectory);
                    var fileNameOnly = filePathAndName.Substring(index + imageDirectory.Length);
                    if (!imageHistoryNames.Contains(fileNameOnly) && File.Exists(@filePathAndName))
                        File.Delete(@filePathAndName);
                }
            }
            catch (Exception e)
            {
                new Logger().LogException(e);
            }
        }
    }
}
