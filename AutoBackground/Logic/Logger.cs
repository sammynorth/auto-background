﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AutoBackground.Logic
{
    public class Logger
    {
        public void LogException(Exception e)
        {
            Directory.CreateDirectory("logs");
            if (e != null && !string.IsNullOrWhiteSpace(e.Message))
            {
                var allLines = new List<string> { "***** - " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " - *****", "Message: " + e.Message };
                if (e.InnerException != null && !string.IsNullOrWhiteSpace(e.InnerException.ToString()))
                    allLines.Add(e.InnerException.ToString());
                File.AppendAllLines("logs\\" + DateTime.Now.DayOfWeek.ToString() + ".txt", allLines);
            }
        }
    }
}
