﻿using AutoBackground.Factory;
using AutoBackground.Logic;
using AutoBackground.Logic.Interface;
using AutoBackground.Models;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace AutoBackground
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        ISettingsIO UserSettingsIO { get; }
        ISettingsIO AppSettingsIO { get; }
        public Settings()
        {
            UserSettingsIO = SettingFactory.GetUserSettingsIO();
            AppSettingsIO = SettingFactory.GetAppSettingsIO();
            InitializeComponent();
            LoadConfigValues();
        }

        private void LoadConfigValues()
        {
            txtSubredditName.Text = UserSettingsIO.GetAppSetting(Constants.Settings.SubredditUrls);
            chkAllowNSFW.IsChecked = UserSettingsIO.GetAppSettingAsBool(Constants.Settings.AllowNSFW);
            sldImageHistoryCountLimit.Value = UserSettingsIO.GetAppSettingAsInt(Constants.Settings.ImageHistoryCountLimit);
            sldTimerInterval.Value = UserSettingsIO.GetAppSettingAsInt(Constants.Settings.TimeIntervalMinutes);
            sldNewToFavsRatio.Value = UserSettingsIO.GetAppSettingAsInt(Constants.Settings.FavoritesFrequency);
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void btnManualRun_Click(object sender, RoutedEventArgs e)
        {
            TimerAndTaskManager.StartOrRestartDownloadLogicOnTimer();
        }

        private void btnSaveAndClose_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
            this.Close();
        }

        private void SaveData()
        {
            UserSettingsIO.SaveAppSetting(Constants.Settings.SubredditUrls, txtSubredditName.Text);
            UserSettingsIO.SaveAppSetting(Constants.Settings.AllowNSFW, chkAllowNSFW.IsChecked.ToString());
            UserSettingsIO.SaveAppSetting(Constants.Settings.ImageHistoryCountLimit, GetCurrentHistoryCountLimitSliderValue().ToString());
            UserSettingsIO.SaveAppSetting(Constants.Settings.TimeIntervalMinutes, GetCurrentTimeIntervalSliderValue().ToString());
            UserSettingsIO.SaveAppSetting(Constants.Settings.FavoritesFrequency, GetCurrentNewToFavRatioSliderValue().ToString());
            TimerAndTaskManager.StartOrRestartDownloadLogicOnTimer();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private int GetCurrentHistoryCountLimitSliderValue()
        {
            return Convert.ToInt16(sldImageHistoryCountLimit.Value);
        }

        private int GetCurrentNewToFavRatioSliderValue()
        {
            return Convert.ToInt16(sldNewToFavsRatio.Value);
        }

        private int GetCurrentTimeIntervalSliderValue()
        {
            return Convert.ToInt16(sldTimerInterval.Value);
        }

        private void sldImageHistoryCountLimit_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (lblImageHistoryCountLimit != null)
                lblImageHistoryCountLimit.Content = GetCurrentHistoryCountLimitSliderValue();
        }
        private void sldNewToFavsRatio_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (lblNewToFavsRatioValue != null)
                lblNewToFavsRatioValue.Content = GetCurrentNewToFavRatioSliderValue() + "%";
        }

        private void sldTimerInterval_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (lblTimerInterval != null)
                lblTimerInterval.Content = ConvertMintutesToReadableTimeString(GetCurrentTimeIntervalSliderValue());
        }

        private string ConvertMintutesToReadableTimeString(int currentSliderValue)
        {
            var hours = Convert.ToInt16(Math.Floor(Convert.ToDouble(currentSliderValue / 60)));
            var minutes = currentSliderValue % 60;
            var hourString = "hr";
            if (hours > 1)
                hourString += "s";
            var minuteString = "min";
            if (minutes != 1)
                minuteString += "s";
            return hours + " " + hourString + " " + minutes + " " + minuteString;
        }

        private void btnOpenImageDirectory_Click(object sender, RoutedEventArgs e)
        {
            var imageDirectory = AppSettingsIO.GetPathAppSettingWithDocumentsDirectory(Constants.Settings.ImageDownloadDirectory);
            Process.Start(@imageDirectory);
        }

        private async void btnKeepImage_Click(object sender, RoutedEventArgs e)
        {
            new MoveImageToFavoritesFolder().Execute();
            lblKeepImage.Visibility = Visibility.Visible;
            await Task.Run(() =>
            {
                Thread.Sleep(10000);
            });
            lblKeepImage.Visibility = Visibility.Hidden;
        }

        private void btnOpenFavoritesDirectory_Click(object sender, RoutedEventArgs e)
        {
            var imageDirectory = AppSettingsIO.GetPathAppSettingWithDocumentsDirectory(Constants.Settings.ImageFavoritesDirectory);
            Process.Start(@imageDirectory);
        }
    }
}
