﻿using AutoBackground.Logic;
using AutoBackground.Models;
using Microsoft.Win32;
using System;
using System.Windows;
using MouseButtons = System.Windows.Forms.MouseButtons;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;
using NotifyIcon = System.Windows.Forms.NotifyIcon;

namespace AutoBackground
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static NotifyIcon icon;
        Settings settings;

        public App()
        {
            TimerAndTaskManager.StartOrRestartDownloadLogicOnTimer();
            CreateNewSettingsInstance();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            App.icon = new NotifyIcon();
            icon.Click += new EventHandler(icon_Click);
            icon.Icon = new System.Drawing.Icon(typeof(App), Constants.Settings.IconFileName);
            icon.Text = Constants.Settings.IconText;
            icon.Visible = true;

            SystemEvents.PowerModeChanged += this.SystemEvents_PowerModeChanged;

            base.OnStartup(e);
        }

        private void HandleSettingsClosed(object sender, EventArgs e)
        {
            CreateNewSettingsInstance();
        }

        private void CreateNewSettingsInstance()
        {
            settings = new Settings();
            settings.Closed += HandleSettingsClosed;
        }

        private void SystemEvents_PowerModeChanged(object sender, PowerModeChangedEventArgs e)
        {
            if (e.Mode == PowerModes.Resume)
            {
                TimerAndTaskManager.StartOrRestartDownloadLogicOnTimer();
                CreateNewSettingsInstance();
            }
        }

        private void icon_Click(object sender, EventArgs e)
        {
            var mouseArgs = (MouseEventArgs)e;
            if (mouseArgs.Button == MouseButtons.Right)
            {
                if (settings.Visibility == Visibility.Visible)
                    settings.Activate();
                else
                    settings.Show();
            }
            else
            {
                if (MessageBox.Show("Are you sure you want to close Auto Background Downloader?", "Auto Background Downloader Confirm", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    settings.Close();
                    App.icon.Dispose();
                    Environment.Exit(0);
                }
            }
        }
    }
}
